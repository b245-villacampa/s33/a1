console.log("Hello world!");


// retriving using GET the title of the to do list 
fetch("https://jsonplaceholder.typicode.com/todos", {method:"GET"}).then(response => response.json()).then(result => console.log(result.map(todo =>todo.title)));


// Fetch single to do list item
fetch("https://jsonplaceholder.typicode.com/todos/2", {method:"GET"}).then(response => response.json()).then(result => console.log(`The ${result.title} has a status of ${result.completed} in the list`));

// Fetch using POST to create a to do list
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers:{
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		"title" : "Coding",
		"description" : "To code from 8am - 5pm",
		"status" : "Complete",
		"dateCompleted" : "01/31/2023",
		"userId" : 9
	})
	}).then(response => response.json()).then(result => console.log(result));


// Fetch using PUT and update the item in the to do list
 fetch("https://jsonplaceholder.typicode.com/todos/2", {
 	method: "PUT",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		"title" : "Play Volleyball",
		"description" : "To play with you friends from 5pm - 9pm",
		"status" : "Pending",
		"dateCompleted" : "Pending",
		"userId" : 11
	})
	}).then(response => response.json()).then(result =>console.log(result));


// Fetch using PATCH and update a to do list item
 fetch("https://jsonplaceholder.typicode.com/todos/50", {
 	method: "PATCH",
 	headers:{
 		"Content-Type" : "application/json"
 	},
 	body:JSON.stringify({
 		"status" : "Complete",
 		"statusChanged" : "02/14/2023"
 	})
}).then(response => response.json()).then(result => console.log(result));


// Fetch and DELETE
 fetch("https://jsonplaceholder.typicode.com/todos/89", {method:"DELETE"}).then(response => response.json()).then(result => console.log(result));